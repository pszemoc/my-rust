extern crate liblist;

use liblist::*;
use rand::Rng;

fn main() {
  let mut l = list::<i32>::new();
  for i in 0..20 {
    l.push_back(rand::thread_rng().gen_range(-100,101));
    println!("l[{}]: {}", i, l[i]);
  }

  let mut sum: i32 = 0;
  for i in 0..l.get_size() {
    sum+=l[i];
  }

  println!("Sum: {}", sum);

  let avg = sum as f64 / l.get_size() as f64;

  println!("Average: {}", avg);
}
