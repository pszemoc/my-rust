extern crate libcalculator;

use libcalculator::*;


fn main() {
  let mut c = calculator::new();
  c.add(10.0, 5.0);
  println!("{}", c.get_result());

  match c.divide(20.0, 0.0) {
    status::ok => println!("{}", c.get_result()),
    status::division_by_zero => println!("divide error"),
    _ => (),
  }

  let mut x1: f64 = 0.0;
  let mut x2: f64 = 0.0;
  c.square_roots_ref(1.0, 0.0, -3.0, &mut x1, &mut x2);
  println!("{}", x1);
  println!("{}", x2);
  let (xr1, xr2, _) = c.square_roots_ret(1.0, 1.0, -4.0);
  println!("{}", xr1);
  println!("{}", xr2);
}
