#![allow(non_camel_case_types)]

pub struct calculator {
  result: f64,
}

#[derive(PartialEq)]
pub enum status {
  ok,
  division_by_zero,
  constant_function,
  linear_function,
  no_roots,
  one_root,
  two_roots,
}

impl calculator {
  pub fn new() -> calculator {
    calculator { result: 0.0 }
  }

  pub fn add(&mut self, x: f64, y: f64) {
    self.result = x + y;
  }

  pub fn subtract(&mut self, x: f64, y: f64) {
    self.result = x - y;
  }

  pub fn multiply(&mut self, x: f64, y: f64) {
    self.result = x * y;
  }

  pub fn divide(&mut self, x: f64, y: f64) -> status {
    if y == 0.0 {
      return status::division_by_zero;
    } else {
      self.result = x / y;
      return status::ok;
    }
  }

  pub fn get_result(&self) -> f64 {
    self.result
  }

  pub fn square_roots_ref(&self, a: f64, b: f64, c: f64, x1: &mut f64, x2: &mut f64) -> status {
    if a == 0.0 {
      if b == 0.0 {
        return status::constant_function;
      }
      *x1 = -c / b;
      return status::linear_function;
    } else {
      let delta = b * b - 4.0 * a * c;
      if delta < 0.0 {
        return status::no_roots;
      } else if delta == 0.0 {
        *x1 = -b / (2.0 * a);
        *x2 = -b / (2.0 * a);
        return status::one_root;
      } else {
        *x1 = (-b - delta.sqrt()) / (2.0 * a);
        *x2 = (-b + delta.sqrt()) / (2.0 * a);
        return status::two_roots;
      }
    }
  }

  pub fn square_roots_ret(&self, a: f64, b: f64, c: f64) -> (f64, f64, status) {
    let mut x1: f64 = 0.0;
    let mut x2: f64 = 0.0;
    let status = self.square_roots_ref(a, b, c, &mut x1, &mut x2);
    return (x1, x2, status);
  }
}
