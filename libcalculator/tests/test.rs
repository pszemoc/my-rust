use libcalculator::{calculator, status};

#[test]
fn addition() {
  let mut c = calculator::new();
  c.add(2.0, 2.0);
  assert_eq!(4.0, c.get_result());
}

#[test]
fn subtraction() {
  let mut c = calculator::new();
  c.subtract(2.0, 2.0);
  assert_eq!(0.0, c.get_result());
}

#[test]
fn multiplication() {
  let mut c = calculator::new();
  c.multiply(3.0, 2.0);
  assert_eq!(6.0, c.get_result());
}

#[test]
fn division() {
  let mut c = calculator::new();
  if c.divide(3.0, 2.0) != status::ok {
    panic!("division error");
  }
  assert_eq!(1.5, c.get_result());
}

#[test]
fn division_by_zero() {
  let mut c = calculator::new();
  let st = c.divide(3.0, 0.0);
  assert!(matches!(st, status::division_by_zero));
}

#[test]
fn square_fun_ref_ok() {
  let mut x1: f64 = 0.0;
  let mut x2: f64 = 0.0;
  let c = calculator::new();
  let status = c.square_roots_ref(1.0, -1.0, -6.0, &mut x1, &mut x2);
  if x1 != -2.0 {
    panic!("wrong roots")
  }
  if x2 != 3.0 {
    panic!("wrong roots")
  }
  assert!(matches!(status, status::two_roots));
}

#[test]
fn square_fun_ret_ok() {
  let c = calculator::new();
  let (x1, x2, status) = c.square_roots_ret(1.0, -1.0, -6.0);
  if x1 != -2.0 {
    panic!("wrong roots")
  }
  if x2 != 3.0 {
    panic!("wrong roots")
  }
  assert!(matches!(status, status::two_roots));
}

#[test]
fn square_fun_ref_one() {
  let mut x1: f64 = 0.0;
  let mut x2: f64 = 0.0;
  let c = calculator::new();
  let status = c.square_roots_ref(1.0, -6.0, 9.0, &mut x1, &mut x2);
  if x1 != 3.0 {
    panic!("wrong roots")
  }
  assert!(matches!(status, status::one_root));
}

#[test]
fn square_fun_ret_one() {
  let c = calculator::new();
  let (x1, _, status) = c.square_roots_ret(1.0, -6.0, 9.0);
  if x1 != 3.0 {
    panic!("wrong roots")
  }
  assert!(matches!(status, status::one_root));
}

#[test]
fn square_fun_ref_none() {
  let mut x1: f64 = 0.0;
  let mut x2: f64 = 0.0;
  let c = calculator::new();
  let status = c.square_roots_ref(1.0, 0.0, 1.0, &mut x1, &mut x2);
  assert!(matches!(status, status::no_roots));
}

#[test]
fn square_fun_ret_none() {
  let c = calculator::new();
  let (_, _, status) = c.square_roots_ret(1.0, 0.0, 1.0);
  assert!(matches!(status, status::no_roots));
}

#[test]
fn square_fun_ref_lin() {
  let mut x1: f64 = 0.0;
  let mut x2: f64 = 0.0;
  let c = calculator::new();
  let status = c.square_roots_ref(0.0, 1.0, 1.0, &mut x1, &mut x2);
  if x1 != -1.0 {
    panic!("wrong roots")
  }
  assert!(matches!(status, status::linear_function));
}

#[test]
fn square_fun_ret_lin() {
  let c = calculator::new();
  let (x1, _, status) = c.square_roots_ret(0.0, 1.0, 1.0);
  if x1 != -1.0 {
    panic!("wrong roots")
  }
  assert!(matches!(status, status::linear_function));
}

#[test]
fn square_fun_ref_const() {
  let mut x1: f64 = 0.0;
  let mut x2: f64 = 0.0;
  let c = calculator::new();
  let status = c.square_roots_ref(0.0, 0.0, 1.0, &mut x1, &mut x2);
  assert!(matches!(status, status::constant_function));
}

#[test]
fn square_fun_ret_const() {
  let c = calculator::new();
  let (_, _, status) = c.square_roots_ret(0.0, 0.0, 1.0);
  assert!(matches!(status, status::constant_function));
}

