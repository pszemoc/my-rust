use liblist::*;

#[test]
fn list_create_correctly() {
  let _l = list::<i32>::new();
}

#[test]
fn push_back_correctly() {
  let mut l = list::<i32>::new();
  l.push_back(10);
  assert_eq!(l.get_size(), 1);
}

#[test]
fn push_back_twice_correctly() {
  let mut l = list::<i32>::new();
  l.push_back(10);
  l.push_back(10);
  assert_eq!(l.get_size(), 2);
}

#[test]
fn get_item_at_correctly() {
  let mut l = list::<i32>::new();
  for x in 0..10 {
    l.push_back(x);
  }
  let result: i32;
  if let Ok(i) = l.at(9) {
    result = *i;
  } else {
    panic!("list.at returned error");
  }
  assert_eq!(result, 9);
}

#[test]
fn error_on_out_of_bonds_at() {
  let mut l = list::<i32>::new();
  for x in 0..10 {
    l.push_back(x);
  }

  if let Err(_) = l.at(100) {
    assert!(true);
  } else {
    panic!("at doesnt return error!")
  }
}

#[test]
fn clones_correctly() {
  let mut l = list::<i32>::new();
  for x in 0..10 {
    l.push_back(x);
  }

  let c = l.clone();
  if c.get_size() != 10 {
    panic!("Invalid copy size! Got size: {}", l.get_size());
  }
  for x in 0..10 {
    if x as i32 != *c.at(x).unwrap() {
      panic!("Invalid copy!");
    }
  }
}

#[test]
fn joins_correctly() {
  let mut l1 = list::<i32>::new();
  let mut l2 = list::<i32>::new();
  for x in 0..10 {
    l1.push_back(x);
  }

  for x in 10..20 {
    l2.push_back(x);
  }

  l1.join(&l2);

  if l1.get_size() != 20 {
    panic!("Invalid join size! Got size: {}", l1.get_size());
  }
  for x in 0..20 {
    if x as i32 != *l1.at(x).unwrap() {
      panic!("Invalid join!");
    }
  }
}

#[test]
fn adds_correctly() {
  let mut l1 = list::<i32>::new();
  let mut l2 = list::<i32>::new();
  for x in 0..10 {
    l1.push_back(x);
  }

  for x in 10..20 {
    l2.push_back(x);
  }

  let result = l1 + l2;

  if result.get_size() != 20 {
    panic!("Invalid join size! Got size: {}", result.get_size());
  }
  for x in 0..20 {
    if x as i32 != *result.at(x).unwrap() {
      panic!("Invalid join!");
    }
  }
}


#[test]
fn adds_assign_correctly() {
  let mut l1 = list::<i32>::new();
  let mut l2 = list::<i32>::new();
  for x in 0..10 {
    l1.push_back(x);
  }

  for x in 10..20 {
    l2.push_back(x);
  }

  l1 += l2;

  if l1.get_size() != 20 {
    panic!("Invalid join size! Got size: {}", l1.get_size());
  }
  for x in 0..20 {
    if x as i32 != *l1.at(x).unwrap() {
      panic!("Invalid join!");
    }
  }
}


#[test]
fn mulls_correctly() {
  let mut l1 = list::<i32>::new();
  for x in 0..10 {
    l1.push_back(x);
  }

  let result = l1*5;

  if result.get_size() != 50 {
    panic!("Invalid join size! Got size: {}", result.get_size());
  }
  for x in 0..50 {
    let value = *result.at(x).unwrap();
    if (x as i32) % 10 != value {
      panic!("Invalid join! Got: {}, expected: {}", value, x);
    }
  }
}

#[test]
fn get_by_index_correctly() {
  let mut l1 = list::<i32>::new();
  for x in 0..10 {
    l1.push_back(x);
  }

  assert_eq!(l1[5], 5);
}

#[test]
fn assign_by_index_correcty() {
  let mut l1 = list::<i32>::new();
  for x in 0..10 {
    l1.push_back(x);
  }

  l1[5] = 20;
  assert_eq!(l1[5], 20);
}