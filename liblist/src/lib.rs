#![allow(non_camel_case_types)]
extern crate libc;

use std::ptr::null_mut;
use std::mem;

pub struct list<T: Clone> {
  head: *mut node<T>,
  size: u64,
}

struct node<T: Clone> {
  item: T,
  next: *mut node<T>,
}

impl<T: Clone> node<T> {
  pub fn new(t: T) -> node<T> {
    node{item: t, next: null_mut()}
  }
}

impl<T: Clone> Drop for list<T> {
  fn drop(&mut self) {
    while self.size > 0 {
      self.pop_back().ok();
    }
  }
}

impl<T: Clone> Clone for list<T> {
  fn clone(&self) -> list<T> {
    let mut copy = list::<T>::new();
    let mut it: *mut node<T> = self.head;
    unsafe {
      while it != null_mut() {
        let item = (*it).item.clone();
        copy.push_back(item);
        it = (*it).next;
      }
    }
    copy
  }
}

impl<T: Clone> list<T> {
  pub fn new() -> list<T> {
    list{head: null_mut(), size: 0}
  }

  pub fn push_back(&mut self, t: T) {
    if self.head == null_mut() {
      unsafe{
        self.head = libc::malloc(mem::size_of::<node<T>>()) as *mut node<T>;
        *self.head = node::new(t);
      }
    } else {
      let mut it: *mut node<T> = self.head;
      unsafe {
        while (*it).next != null_mut() {
          it = (*it).next;
        }
        (*it).next = libc::malloc(mem::size_of::<node<T>>()) as *mut node<T>;
        *(*it).next = node::new(t);
      }
    }
    self.size+=1;
  }

  pub fn pop_back(&mut self) -> Result<(), &str> {
    if self.size == 0 {
      return Err("List is empty!");
    }
    if self.size == 1 {
      unsafe {
        libc::free(self.head as *mut libc::c_void);
        self.head = null_mut();
      }
    } else {
      let mut it: *mut node<T> = self.head;
      unsafe {
        while (*(*it).next).next != null_mut() {
          it = (*it).next;
        }
        libc::free((*it).next as *mut libc::c_void);
        (*it).next = null_mut();
      }
    }
    self.size-=1;
    Ok(())
  }

  pub fn at(&self, idx: u64) -> Result<&T, &str> {
    if idx >= self.size {
      return Err("Index out of bonds!");
    }
    let mut it: *mut node<T> = self.head;
    unsafe {
      for _ in 0..idx {
        it = (*it).next;
      }
      Ok(&(*it).item)
    }
  }

  pub fn insert(&mut self, t: T, idx: u64) -> Result<(), &str> {
    if idx >= self.size {
      return Err("Index out of bonds!");
    }
    if idx == 0 {
      if self.head == null_mut() {
        unsafe{
          self.head = libc::malloc(mem::size_of::<node<T>>()) as *mut node<T>;
          *self.head = node::new(t);
          return Ok(());
        }
      }
      let second: *mut node<T> = self.head;
      unsafe {
        self.head = libc::malloc(mem::size_of::<node<T>>()) as *mut node<T>;
        *self.head = node::new(t);
        (*self.head).next = second;
      }
      self.size+=1;
      return Ok(());
    }
    if idx == self.size {
      self.push_back(t);
      return Ok(());
    }

    let mut it: *mut node<T> = self.head;
    unsafe {
      for _ in 0..idx {
        it = (*it).next;
      }
    }

    unsafe {
      let mut new_node: *mut node<T> = libc::malloc(mem::size_of::<node<T>>()) as *mut node<T>;
      (*new_node).next = (*it).next;
      (*it).next = new_node;
    }

    self.size+=1;
    Ok(())
  }

  pub fn join(&mut self, l: &list<T>) {
    let clone = l.clone();
    let mut it: *mut node<T> = clone.head;
    unsafe {
      while it != null_mut() {
        let item = (*it).item.clone();
        self.push_back(item);
        it = (*it).next;
      }
    }
  }

  pub fn get_size(&self) -> u64 {
    self.size
  }
}

impl<T: Clone> std::ops::Add for list<T> {
  type Output = Self;

  fn add(self, rhs: Self) -> Self {
    let mut clone = self.clone();
    clone.join(&rhs);
    clone
  }
}

impl<T: Clone> std::ops::AddAssign for list<T> {
  fn add_assign(&mut self, rhs: Self) {
    self.join(&rhs);
  }
}

impl<T: Clone> std::ops::Mul<u64> for list<T> {
  type Output = Self;
  fn mul(self, rhs: u64) -> Self {
    let mut clone = self.clone();
    let mut x = rhs;
    while x > 1 {
      clone.join(&self);
      x-=1;
    }
    clone
  }
}

impl<T: Clone> std::ops::Index<u64> for list<T> {
  type Output = T;
  fn index(&self, idx: u64) -> &Self::Output {
    if let Ok(value) = self.at(idx) {
      value
    } else {
      panic!("Index out of bonds!");
    }
  }
}

impl<T: Clone> std::ops::IndexMut<u64> for list<T> {
  fn index_mut(&mut self, idx: u64) -> &mut T {
    if idx >= self.size {
      panic!("Index out of bonds!");
    }
    let mut it: *mut node<T> = self.head;
    unsafe {
      for _ in 0..idx {
        it = (*it).next;
      }
      &mut (*it).item
    }

  }
}



#[cfg(test)]
mod internal_tests {
  use std::ptr::null_mut;
  use super::*;

  #[test]
  fn list_create_correctly() {
    let l = list::<i32>::new();
    if l.head != null_mut() {
      panic!("head not null");
    }
    assert_eq!(0, l.size);
  }

  #[test]
  fn push_back_correctly() {
    let mut l = list::<i32>::new();
    l.push_back(10);
    if l.head == null_mut() {
      panic!("head null");
    }
    unsafe {
      if (*l.head).item != 10 {
        panic!("wrong item");
      }
      if (*l.head).next != null_mut() {
        panic!("next not null");
      }
    }
    assert_eq!(l.size, 1);
  }

  #[test]
  fn push_back_twice_correctly() {
    let mut l = list::<i32>::new();
    l.push_back(10);
    l.push_back(10);
    unsafe {
      if (*l.head).next == null_mut() {
        panic!("next null");
      }
      if (*(*l.head).next).item != 10 {
        panic!("wrong item");
      }
      if (*(*l.head).next).next != null_mut() {
        panic!("next not null");
      }
    }
    assert_eq!(l.size, 2);
  }

  #[test]
  fn pop_back_on_one_element_list_correctly() {
    let mut l = list::<i32>::new();
    l.push_back(10);

    if let Ok(_) = l.pop_back() {
      if l.head != null_mut() {
        panic!("head not null");
      }
      assert_eq!(l.size, 0);
    } else {
      panic!("pop_back returned error!");
    }
  }

  #[test]
  fn pop_back_on_two_element_list_correctly() {
    let mut l = list::<i32>::new();
    l.push_back(10);
    l.push_back(10);

    if let Ok(_) = l.pop_back() {
      unsafe {
        if (*l.head).next != null_mut() {
          panic!("next not null");
        }
      }
      assert_eq!(l.size, 1);
    } else {
      panic!("pop_back returned error!");
    }
  }

  #[test]
  fn empty_after_drop() {
    let mut l = list::<i32>::new();
    for x in 0..10 {
      l.push_back(x);
    }
    mem::drop(l);
  }
}
